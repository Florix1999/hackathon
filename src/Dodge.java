import org.newdawn.slick.*;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;
import org.newdawn.slick.tests.SoundTest;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Dodge extends BasicGameState {
    private Image hintergrund;
    private Mensch mensch1;
    private Mensch mensch2;
    private Sound gameOver;
    private Sound music;
    private int SpeedMensch = 1;
    private int random;
    private int a;
    private boolean multi = false;

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public int score;
    private Image gameover;
    private Image gameover2;
    private boolean gamerun;
    private boolean gamerun2;
    private boolean collision;
    private ArrayList <Hinderniss> hindernisse;

    public Dodge(int state) {

    }


    @Override
    public void render(GameContainer gameContainer, StateBasedGame stateBasedGame, Graphics g) throws SlickException {
        hintergrund.draw();
        for (Hinderniss hinderniss : hindernisse) {
            hinderniss.draw(g);
            mensch1.draw(g);
            if (multi) {
                mensch2.draw(g);
            }
            if (gamerun == false){
                gameover.draw();
                this.collision = false;
            }
            if (gamerun2 == false){
                gameover2.draw();
                this.collision = false;
            }
            g.setColor(Color.red);
            g.drawString(String.valueOf(this.score),10, 10);
        }
    }

    public void setMultiplayer(boolean multi) {
        this.multi = multi;
    }

    @Override
    public void init(GameContainer gameContainer, StateBasedGame stateBasedGame) throws SlickException {
        score = 0;
        hindernisse = new ArrayList<>(10);
        gamerun = true;
        gamerun2 = true;
        collision = true;
        Random r = new Random();

        hintergrund = new Image("res/Spielfigur und Hintergrund/Back1.jpg").getScaledCopy(1920,1080);
        this.gameOver = new Sound("res/sounds/Game over.wav");
        this.a = 400;
        int randomTemp =  r.nextInt(1024);
        random = r.nextInt(a);
        Hinderniss hinderniss1 = new Hinderniss(randomTemp, random - a, new Image("res/Hinderniss/Holz.png"));
        System.out.println("random 1: " + randomTemp);
        hindernisse.add(hinderniss1);


        randomTemp =  r.nextInt(1024);
        random = r.nextInt(a);
        Hinderniss hinderniss2 = new Hinderniss(randomTemp, random - a, new Image("res/Hinderniss/Hubschrauber.png"));
        System.out.println("random 2: " + randomTemp);
        hindernisse.add(hinderniss2);

      /*  randomTemp =  r.nextInt(1024);
        random = r.nextInt(a);
        Hinderniss hinderniss3 = new Hinderniss(randomTemp,  random - a, new Image("res/Hinderniss/Axt.png"));
        System.out.println("random 3: " + randomTemp);
        hindernisse.add(hinderniss3);*/

        randomTemp =  r.nextInt(1024);
        random = r.nextInt(a);
        Hinderniss hinderniss4 = new Hinderniss(randomTemp,  random - a, new Image("res/Hinderniss/Heißluftballon.png"));
        System.out.println("random 4: " + randomTemp);
        hindernisse.add(hinderniss4);

        randomTemp =  r.nextInt(1024);
        random = r.nextInt(a);
        Hinderniss hinderniss5 = new Hinderniss(randomTemp,  random - a, new Image("res/Hinderniss/Holzgeschnitten.png"));
        System.out.println("random 5: " + randomTemp);
        hindernisse.add(hinderniss5);

        randomTemp =  r.nextInt(1024);
        random = r.nextInt(a);
        Hinderniss hinderniss6 = new Hinderniss(randomTemp,  random - a, new Image("res/Hinderniss/Hubschrauber.png"));
        System.out.println("random 6: " + randomTemp);
        hindernisse.add(hinderniss6);

       /* randomTemp =  r.nextInt(1024);
        random = r.nextInt(a);
        Hinderniss hinderniss7 = new Hinderniss(randomTemp,  random - a, new Image("res/Hinderniss/HomerSimpson.png"));
        System.out.println("random 7: " + randomTemp);
        hindernisse.add(hinderniss7);*/

       /* randomTemp =  r.nextInt(1024);
        random = r.nextInt(a);
        Hinderniss hinderniss8 = new Hinderniss(randomTemp,  random - a, new Image("res/Hinderniss/Komet.png"));
        System.out.println("random 8: " + randomTemp);
        hindernisse.add(hinderniss8);*/

        randomTemp =  r.nextInt(1024);
        random = r.nextInt(a);
        Hinderniss hinderniss9 = new Hinderniss(randomTemp,  random - a, new Image("res/Hinderniss/Tanne.png"));
        System.out.println("random 9: " + randomTemp);
        hindernisse.add(hinderniss9);

        randomTemp =  r.nextInt(1024);
        random = r.nextInt(a);
        Hinderniss hinderniss10 = new Hinderniss(randomTemp,  random - a, new Image("res/Hinderniss/Vogel.png").getScaledCopy(250,250));
        System.out.println("random 10: " + randomTemp);
        hindernisse.add(hinderniss10);



        mensch1 = new Mensch(512, 700, new Image("res/Spielfigur und Hintergrund/Spielfigurgraus.png"));
        mensch2 = new Mensch(512, 700, new Image("res/Spielfigur und Hintergrund/Spielfigur2.png"));

        gameover = new Image("res/Spielfigur und Hintergrund/Game Over.png");
        gameover2 = new Image("res/Spielfigur und Hintergrund/Gameover2.png");
        this.music = new Sound("res/sounds/Backgroundc.wav");
        music.loop();
    }

    @Override
    public void update(GameContainer container, StateBasedGame stateBasedGame, int delta) throws SlickException {
        if(this.collision == true) {
            if(getScore() == 5000){
                for (int i = 0; i < hindernisse.size(); i++){
                    hindernisse.get(i).setSpeed(2);
                }
                this.SpeedMensch = 2;
            }
            else if (getScore () == 12000){
                for (int i = 0; i < hindernisse.size(); i++){
                    hindernisse.get(i).setSpeed(3);
                }
                this.SpeedMensch = 3;
            }
            else if (getScore () == 20000){
                for (int i = 0; i < hindernisse.size(); i++){
                    hindernisse.get(i).setSpeed(4);
                }
                this.SpeedMensch = 4;
            }
            else if (getScore () == 25000){
                for (int i = 0; i < hindernisse.size(); i++){
                    hindernisse.get(i).setSpeed(5);
                }
                this.SpeedMensch = 5;
            }
            else if (getScore () == 30000){
                for (int i = 0; i < hindernisse.size(); i++){
                    hindernisse.get(i).setSpeed(6);
                }
                this.SpeedMensch = 6;
            }
            else if (getScore () == 36000){
                for (int i = 0; i < hindernisse.size(); i++){
                    hindernisse.get(i).setSpeed(7);
                }
                this.SpeedMensch = 7;
            }
            else if (getScore () == 45000){
                for (int i = 0; i < hindernisse.size(); i++){
                    hindernisse.get(i).setSpeed(8);
                }
                this.SpeedMensch = 8;
            }

            this.score++;
            for (Hinderniss hinderniss : hindernisse) {
                hinderniss.update(delta);

                if (hinderniss.pruefeKollsion(mensch1)) {
                    System.out.println("kollision1 entdeckt ...");
                    gamerun = false;
                    music.stop();
                    gameOver.play();

                }

                if (multi && hinderniss.pruefeKollsion(mensch2)) {
                    System.out.println("kollision2 entdeckt ...");
                    gamerun2 = false;
                    music.stop();
                    gameOver.play();


                }

            }
        }

        if (container.getInput().isKeyDown(Input.KEY_ESCAPE)) {
            container.exit();
        }

        if (container.getInput().isKeyDown(Input.KEY_ENTER)) {
            stateBasedGame.enterState(0);
        }

        mensch1.update(delta);
        mensch2.update(delta);

        if (container.getInput().isKeyDown(Input.KEY_D)) {
            mensch1.setX(mensch1.getX() + SpeedMensch);
        }
        if (container.getInput().isKeyDown(Input.KEY_A)) {
            mensch1.setX(mensch1.getX() - SpeedMensch);
        }

        if (container.getInput().isKeyDown(Input.KEY_RIGHT)) {
            mensch2.setX(mensch2.getX() + SpeedMensch);
        }
        if (container.getInput().isKeyDown(Input.KEY_LEFT)) {
            mensch2.setX(mensch2.getX() - SpeedMensch);
        }
    }

    @Override
    public int getID() {
        return 1;
    }
}