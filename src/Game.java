import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

/**
 * Created by Jonas M on 31.03.2017.
 */
public class Game extends StateBasedGame {

    public static final int menu = 0;
    public static final int dodge = 1;

    public Game(String name) {
        super(name);
        this.addState(new Menu(menu));
        this.addState(new Dodge(dodge));
    }

    @Override
    public void initStatesList(GameContainer gameContainer) throws SlickException {
        //this.getState(menu).init(gameContainer, this);
        //this.getState(dodge).init(gameContainer, this);
        this.enterState(menu);
    }

    public static void main(String[] args) throws SlickException {
        AppGameContainer container = new AppGameContainer(new Game("dodge"));
        container.setDisplayMode(1024, 768, false);
        container.setShowFPS(false);
        container.setClearEachFrame(false);
        container.setMinimumLogicUpdateInterval(4);
        container.start();
    }
}
