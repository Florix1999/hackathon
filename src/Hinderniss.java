import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.geom.Ellipse;
import org.newdawn.slick.geom.Shape;

import java.util.Random;

public class Hinderniss extends SpielObjekt {
    private Random random;
    private int speed;

    public Hinderniss(int x, int y, Image image) {
        super(x, y, image);
        this.speed = 1;
        kollisionsFlaeche = new Ellipse(x, y, 100, 50);
    }

    @Override
    public void update(int delta) {

        if (getY() > 768){
            random = new Random();
            setY(random.nextInt(400) - 400);
            setX(random.nextInt(1024));
        }

        setY(getY() + speed);
        kollisionsFlaeche.setCenterX(getX());
        kollisionsFlaeche.setCenterY(getY());
    }

    @Override
    public void draw(Graphics g) {
        getImage().drawCentered(getX(),getY());


    }


    public Shape kollisionsFlaeche;


    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public boolean pruefeKollsion(SpielObjekt spielObjekt) {
        return kollisionsFlaeche.contains(spielObjekt.getX(),
                spielObjekt.getY());
    }

}