import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.geom.Ellipse;
import org.newdawn.slick.geom.Shape;

/**
 * Created by Madlener Jonas on 22.12.2016.
 */
public class Mensch extends SpielObjekt {

    private double geschwindigkeitX = 2;
    private double geschwindigkeitY = 0;
    private double beschleunigung = 0.0;


    /**
     * Erzeugt ein neues Mensch mit Übergabewerten / Ruft den Konstruktur der Basisklasse auf (--> super)
     *
     * @param x     x-Koordinate
     * @param y     y-Koordinate
     * @param image Graphik des Spielobjekt
     */
    public Mensch(int x, int y, Image image) {
        super(x, y, image);

    }


    /**
     * Wird bei jedem Spieldurchgang (Spiel-loop) aufgerufen
     *
     * @param delta Zeitdifferenz zum letzten Aufruf
     */
    @Override
    public void update(int delta) {
       if (getX() > 1024) {
            setX(0);
        } else if (getX() < 0) {
           setX(1024);
        }
    }

    /**
     * Zeichnet das Spielobjekt
     *
     * @param g Zugriff auf die Graphik des Spiels
     */
    @Override
    public void draw(Graphics g) {
        getImage().drawCentered(getX(), getY());
    }

    /**
     * Geschwindigkeit ändern
     *
     * @param vXNew neue Geschwindigkeit in x-Richtung (y=0)
     */
    public void updateVX(int vXNew) {
        geschwindigkeitX = vXNew;
        geschwindigkeitY = 0;
    }


    @Override
    public void setX(int x) {
        super.setX(x);
    }

    @Override
    public int getX() {
        return super.getX();
    }

    @Override
    public void setY(int y) {
        super.setY(y);
    }

    @Override
    public int getY() {
        return super.getY();
    }
}

