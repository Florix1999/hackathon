import org.lwjgl.input.Mouse;
import org.newdawn.slick.*;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

/**
 * Created by Jonas M on 31.03.2017.
 */

public class Menu extends BasicGameState {

    Image singlePlayer;
    Image multiPlayer;

    int buttonWidth = 303;
    int buttonHeight = 72;

    int singlePlayerX = 200;
    int singlePlayerY = 200;

    int multiPlayerX = 200;
    int multiPlayerY = 300;

    public Menu(int state) {

    }

    @Override
    public int getID() {
        return 0;
    }

    @Override
    public void init(GameContainer gameContainer, StateBasedGame stateBasedGame) throws SlickException {
        singlePlayer = new Image("res/Spielfigur und Hintergrund/Single Player.png");
        multiPlayer = new Image("res/Spielfigur und Hintergrund/Multi Player.png");
    }

    @Override
    public void render(GameContainer gameContainer, StateBasedGame stateBasedGame, Graphics graphics) throws SlickException {
        //graphics.drawString("Welcome to Dodge!", 100, 50);
        graphics.setColor(new Color(0, 242, 255));
        graphics.fill(new Rectangle(0, 0, gameContainer.getWidth(), gameContainer.getHeight()));
        singlePlayer.draw(singlePlayerX, singlePlayerY);
        multiPlayer.draw(multiPlayerX, multiPlayerY);
    }

    @Override
    public void update(GameContainer gameContainer, StateBasedGame stateBasedGame, int i) throws SlickException {
        int posX = Mouse.getX();
        int posY = gameContainer.getHeight() - Mouse.getY();
        System.out.println("mouse: " + posX + " - " + posY);
        if ((posX > singlePlayerX && posX < singlePlayerX + buttonWidth) &&
                (posY > singlePlayerY && posY < singlePlayerY + buttonHeight)) {
            if (Mouse.isButtonDown(0)){
                Dodge dodge = (Dodge) stateBasedGame.getState(1);
                dodge.setMultiplayer(false);
                stateBasedGame.enterState(1);
                stateBasedGame.getState(1).init(gameContainer, stateBasedGame);
            }

        }
        if ((posX > multiPlayerX && posX < multiPlayerX + buttonWidth) &&
                (posY > multiPlayerY && posY < multiPlayerY + buttonHeight)) {
            if (Mouse.isButtonDown(0)){
                Dodge dodge = (Dodge) stateBasedGame.getState(1);
                dodge.setMultiplayer(true);
                stateBasedGame.enterState(1);
                stateBasedGame.getState(1).init(gameContainer, stateBasedGame);
            }

        }

    }
}
